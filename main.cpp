#include "drzewo.h"
using namespace std;

int main() {
	Tree<int> A, B;	
	A += 5;
	A += 10;
	A += 15;
	cout << A;
	B = A; 
	cout << B;
	if (A == B) cout << "Drzewa sa sobie rowne!" << endl;
	if (A != B) cout << "Drzewa sa rozne!" << endl;
}
