CC	= g++
CFLAGS	= -c -g -Wall  
EXE	= prog_test
SOURCES = main.cpp drzewo.cpp
OBJECTS = $(SOURCES:.cpp=.o)
CLEAN	= rm -rf 

$(EXE): $(OBJECTS) 
	$(CC) $(OBJECTS) -o $@

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp 
drzewo.o: drzewo.cpp
	$(CC) $(CFLAGS) drzewo.cpp 

clean:
	$(CLEAN) $(OBJECTS) $(EXE)

