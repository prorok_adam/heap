#ifndef HEAP_CPP
#define HEAP_CPP
// Adam Nowik J2I3
// Zbior obslugujacy dowolny typ danych - drzewo
#include "drzewo.h"
// KONSTRUKTORY

// konstruktor bezargumentowy
template<class T>
Tree<T>::Tree() {
	root = NULL; // wskaznik na pierwszy element sterty
	last = NULL; // wskaznik na ostatni element sterty
	amount = 0; // ilosc elementow w stercie
}

// konstruktor jednoargumentowy
template<class T>
Tree<T>::Tree(T nowy) {
	root = new Knot;
	root->parent = NULL; // wskaźnik na element poprzedni
	root->l_child = NULL; // wskaźnik na lewy element następny
	root->r_child = NULL; // wskaźnik na prawy element następny
	root->elem = nowy; // przechowywany element
	root->number = 1; // numer elementu
	amount = 1; // ilosc elementow w stercie
	last = root; // root jest ostatnim elementem
}
// konstruktor kopiujacy
template<class T>
Tree<T>::Tree(const Tree<T>& source) {
	Knot* temp1; 
	Knot* temp2;
	if (source.root == NULL) { // jesli kopiujemy pusta sterte
		root = NULL;
		amount = 0;
	} else { // jesli sterta zawiera jakies elementy
		temp2 = source.root;
		temp1 = new Knot;
		temp1->parent = NULL;
		temp1->l_child = NULL;
		temp1->r_child = NULL;
		temp1->elem = temp2->elem;
		temp1->number = 1;
		root = temp1;
		amount = source.amount;
		last = root;
		if (temp2->l_child != NULL)	copy_all(&root->l_child, temp2->l_child, root); // wywolanie funkcji kopiujacej reszte sterty
		if (temp2->r_child != NULL) copy_all(&root->r_child, temp2->r_child, root); // poczawszy od wskazanego elementu
		}
};

// destruktor
template<class T>
Tree<T>::~Tree(){
	if (root != NULL) destroy(root); // niszczy element w nawiasie
	root = NULL; // i wszystkie podrzedne jemu elementy
	last = NULL;
}
// OPERATORY
template<class T>
Tree<T>& Tree<T>::operator+=(T target) {
	if (root == NULL) {
		Knot* temp = new Knot;
		temp->parent = NULL;
		temp->l_child = NULL;
		temp->r_child = NULL;
		temp->elem = target;
		temp->number = 1;
		amount = 1;
		root = temp;
		last = root;
	} else {
		++amount; // dodaje jeden do ilosci wezlow
		Knot* nowy = new Knot; // tworze nowy wezel
		nowy->l_child = NULL; 
		nowy->r_child = NULL;
		nowy->elem = target; // wstawiam tam nowa wartosc
		nowy->number = amount; // jego numer jest taki sam jak ilosc wezlow 
		int j = amount; 
		int i = 0;
		while (j > 1) { // obliczam ile wezlow znajduje sie od
			j /= 2; // nowego wezla do root
			i++;
		}
		Knot* temp = root;
		while (i > 1) { // wedruje od root do miejsca gdzie powinien
			j = amount; // byc "doczepiony" nowy wezel
			for (int k = 1; k < i ; k++) j /= 2;
			--i;
			if (temp->l_child->number == j)	temp = temp->l_child;
			else if (temp->r_child->number == j) temp = temp->r_child;
		}		
		if (nowy->number == (temp->number) * 2) temp->l_child = nowy;	
		else if (nowy->number == ((temp->number) * 2) + 1) temp->r_child = nowy;
		nowy->parent = temp;
		last = nowy;
		//sort(nowy); // funkcja sortujaca, przywraca warunek drzewa
	} // jesli nowy element jest wiekszy niz rodzic do ktorego
	return *this; // zostal dodany
}

template<class T>
Tree<T>& Tree<T>::operator+=(const Tree<T>& source) {
	if (source.root != NULL) { // jesli dodajemy sterte pelna
		add_tree(source.root); // wywoluje add_tree
	}
	return *this;
}


template<class T>
Tree<T> Tree<T>::operator+(const Tree<T>& source) const {
	Tree<T> sum;
	sum += *this;
	sum += source;
	return sum;
}

template<class T>
Tree<T>& Tree<T>::operator=(const Tree<T>& source) {
	if (root != NULL) destroy(root);
	Knot* temp1;
	Knot* temp2;
	if (source.root == NULL) {
		root = NULL;
		amount = 0;
	} else {
		temp2 = source.root;
		temp1 = new Knot;
		temp1->parent = NULL;
		temp1->l_child = NULL;
		temp1->r_child = NULL;
		temp1->elem = temp2->elem;
		temp1->number = 1;
		root = temp1;
		amount = source.amount;
		last = root;
		if (temp2->l_child != NULL) copy_all(&root->l_child, temp2->l_child, root);
		if (temp2->r_child != NULL) copy_all(&root->r_child, temp2->r_child, root);
		}
	return *this;
}

template<class T>
int Tree<T>::operator==(const Tree<T>& source) {
	Knot* temp1 = root;
	Knot* temp2 = source.root;
	if (amount == source.amount) { // jesli ilosci el w stertach
		if (equall(temp1, temp2)) // sa takie same, wywoluje equall
		return 1; // jesli wszystkie, kolejne elementy stert sa
	} // takie same, zwraca prawde
	return 0;// w kazdym innym wypadku zwraca falsz
}

template<class T>
int Tree<T>::operator!=(const Tree<T>& source) {
	Knot* temp1 = root;
	Knot* temp2 = source.root;
	if (amount == source.amount) {
		if (equall(temp1, temp2))
		return 0; // dziala odwrotnie do operaotra "=="
	}
	return 1;
}

template<class T>
Tree<T>& Tree<T>::operator--() {
	pop();
	return *this;
}
// METODY KLASY HEAP
template<class T>
void Tree<T>::destroy(Knot *target) { // niszczy kolejne wezly
	if (target->l_child != NULL) destroy(target->l_child); // jesli istnieje lewy lub prawy potomek funkcja wywoluje sie dla nich
	if (target->r_child != NULL) destroy(target->r_child);
	target->parent = NULL;
	target->l_child = NULL;
	target->r_child = NULL;
	Knot* temp = target;
	delete temp; // usuwanie przydzielonego bloku pamieci
};

template<class T>
void Tree<T>::copy_all(Knot** dest, Knot* source, Knot* parent) {
	Knot* temp1 = new Knot; // kopiowanie kolejnych wezlow
	Knot* temp2 = source; // rekurencyjnie
	temp1->parent = parent;
	temp1->l_child = NULL;
	temp1->r_child = NULL;
	temp1->elem = temp2->elem;
	temp1->number = temp2->number;
	*dest = temp1;
	last = temp1;
	if (temp2->l_child != NULL) copy_all(&temp1->l_child, temp2->l_child, temp1);
	if (temp2->r_child != NULL) copy_all(&temp1->r_child, temp2->r_child, temp1);	
}

template<class T>
void Tree<T>::add_tree(Knot* source) {
	*this += source->elem; // dodawanie kolejnych el jednej sterty do drugiej
	if (source->l_child != NULL) add_tree(source->l_child);
	if (source->r_child != NULL) add_tree(source->r_child);
}

/*template<class T>
void Tree<T>::sort(Knot* target) { // sortowanie kolejnych wezlow sterty
	if (target->parent != NULL) {
		if (target->parent->elem < target->elem) {
			T temp = target->elem; // jesli potomek jest wiekszy niz rodzic
			target->elem = target->parent->elem; // zostaja zamienieni
			target->parent->elem = temp;
			sort(target->parent); // wywolanie rek. funkcji dla rodzica
		}
	}
}
*/
template<class T>
void Tree<T>::pop() { // usuwanie elementu ze sterty
	if (amount > 0) {
		--amount; // zmniejszam liczbe el.
		if (amount > 0) {
			root->elem = last->elem; // ostatni el. staje sie podstawa
			if (last->parent->l_child->number == last->number) last->parent->l_child = NULL; // "odczepiam" rodzica od ost. el.
			else if (last->parent->r_child->number == last->number) last->parent->r_child = NULL;
			destroy(last); // niszcze stary ostatni element
			//sort_down(root); // sortuje, przywracam warunek drzewa
			search_last(root); // szukam nowego ost. el.
		} else {
			destroy(root); // jesli drzewo sklada sie tylko z root
			root = NULL; // niszcze go
			last = NULL;
		}
	} else cout << "Sterta jest juz pusta, nie mozna nic z niej usunac!" << endl;
}
/*
template<class T>
void Tree<T>::sort_down(Knot* target) { // sortowanie rodzica z wiekszym z dzieci
	Knot* temp = NULL;
	if (target->l_child != NULL && target->r_child != NULL) {
		if (target->l_child->elem > target->r_child->elem) temp = target->l_child;
		else temp = target->r_child; // wybieranie wiekszego dziecka
	} else {
		if (target->l_child != NULL && target->r_child == NULL) temp = target->l_child;
		if (target->l_child == NULL && target->r_child != NULL) temp = target->r_child; // lub dziecka istniejacego, jesli jest tylko jedno
	}
	if (temp != NULL) { // jesli wybrano ktorekolwiek z dzieci
		if (target->elem < temp->elem) { // jesli jest ono wieksze
			T elem = target->elem; // od rodzica, nastepuje zamiana
			target->elem = temp->elem;
			temp->elem = elem;
			sort_down(temp); // rek. wyw. funkcji dla zamien. potomka
		}
	}
}
*/
template<class T>
void Tree<T>::search_last(Knot* target) {
	if (target->number == amount) last = target;
	else {
		if (target->l_child != NULL) search_last(target->l_child);
		if (target->r_child != NULL) search_last(target->r_child);
	}
}

template<class T>
int Tree<T>::equall(Knot* target, Knot* source) {
	if (target->elem == source->elem) {
		if (target->l_child != NULL && source->l_child != NULL && target->r_child != NULL && source->r_child != NULL && equall(target->l_child, source->l_child) && equall(target->r_child, source->r_child)) return 1; 
		if (target->l_child != NULL && source->l_child != NULL && target->r_child == NULL && source->r_child == NULL && equall(target->l_child, source->l_child)) return 1;
		if (target->l_child == NULL && source->l_child == NULL && target->r_child != NULL && source->r_child != NULL && equall(target->r_child, source->r_child)) return 1;
		if (target->l_child == NULL && source->l_child == NULL && target->r_child == NULL && source->r_child == NULL) return 1;
		return 0;
	} else return 0; // zwraca prawde jesli elementy przech.przez wezly sa takie same oraz el. przechowywane przez ich potomkow sa takie same
}

template<class T>
ostream& operator<<(ostream& out, const Tree<T>& source)
{
	if (source.root == NULL) out << "Sterta jest pusta" << endl;	
	else {	
		out << endl << source.root->elem << endl;
		if (source.root->l_child != NULL) out << source.root->l_child->elem << " | ";
		if (source.root->r_child != NULL) out << source.root->r_child->elem << endl;
		
		if (source.root->l_child != NULL) {
			if (source.root->l_child->l_child != NULL) out  << source.root->l_child->l_child->elem << " | ";
			if (source.root->l_child->r_child != NULL) out  << source.root->l_child->r_child->elem << " || ";
		}
		if (source.root->r_child != NULL) {
			if (source.root->r_child->l_child != NULL) out << source.root->r_child->l_child->elem << " | ";
			if (source.root->r_child->r_child != NULL) out << source.root->r_child->r_child->elem << endl;
		
			if (source.root->l_child->l_child != NULL) {
			if (source.root->l_child->l_child->l_child != NULL) out << source.root->l_child->l_child->l_child->elem << " | ";
			if (source.root->l_child->l_child->r_child != NULL) out << source.root->l_child->l_child->r_child->elem << " || ";
			}
			if (source.root->l_child->r_child != NULL) {
			if (source.root->l_child->r_child->l_child != NULL) out << source.root->l_child->r_child->l_child->elem << " | ";
			if (source.root->l_child->r_child->r_child != NULL) out << source.root->l_child->r_child->r_child->elem << " ||| ";
			}
			if (source.root->r_child->l_child != NULL) {
			if (source.root->r_child->l_child->l_child != NULL) out << source.root->r_child->l_child->l_child->elem << " | ";
			if (source.root->r_child->l_child->r_child != NULL) out << source.root->r_child->l_child->r_child->elem << " || ";
			}
			if (source.root->r_child->r_child != NULL) {
			if (source.root->r_child->r_child->l_child != NULL) out << source.root->r_child->r_child->l_child->elem << " | ";
			if (source.root->r_child->r_child->r_child != NULL) out << source.root->r_child->r_child->r_child->elem << endl;
			}
		}
	}
	out << endl;
	return out; 
}
#endif
