// Adam Nowik J2I3
// Zbior obslugujacy dowolny typ danych - drzewo
#ifndef HEAP_H
#define HEAP_H
#include <iostream>
using namespace std;
template<class T>
class Tree {
private:
	struct Knot { // przechowuje strukture wezla
		Knot* parent; // wezel "rodzic"
		Knot* l_child; // wezel lewy potomek
		Knot* r_child; // wezel prawy potomek
		T elem; // przechowywana wartosc
		int number; // numer wezla (index)
	};
	Knot* root; // wskaznik na pierwszy wezel
	Knot* last; // wskaznik na ostatnio dodany wezel
	int amount; // ilosc wezlow
// metody klasy tree
	void copy_all(Knot**, Knot*, Knot*); // kopiuje cala strukture drzewa poczawszy od wskazanego wezla
	void destroy(Knot*); // niszczy cala strukture drzewa poczawszy od wskazanego wezla
	void add_tree(Knot*); // dodaje jedno drzewo do drugiego
	//void sort(Knot*); // sortuje drzewo "w gore"
	void search_last(Knot*); // szuka nowego ostatniego wezla
	//void sort_down(Knot*); // sortuje drzewo "w dol" (rodzic i wieksze dziecko)
 	int equall(Knot*, Knot*); // sprawdza czy elementy 2 drzew sa takie same
	void pop(); // usuwa pojedynczy element (root) z drzewa
public:
	Tree(); // konstruktory
	Tree(T);
	Tree(const Tree<T>&);
	~Tree(); // destruktor
	Tree<T>& operator+=(T); // operator += dla pojedynczego elementu
	Tree<T>& operator+=(const Tree<T>&); // += dla calego drzewa
	Tree<T> operator+(const Tree<T>&) const; // dodawanie 2 stert;
	Tree<T>& operator-=(T); // operator -= dla pojedynczego elementu
	Tree<T>& operator-=(const Tree<T>&); // -= dla calego drzewa
	Tree<T>& operator=(const Tree<T>&); // przypisanie
	int operator==(const Tree<T>&); // rowne
	int operator!=(const Tree<T>&); // rozne
	Tree<T>& operator--(); // zdejmowanie el. ze sterty
	template<class T2>
	friend ostream& operator<<(ostream&, const Tree<T2>&); // operator wypisania
};
#include "drzewo.cpp"
#endif // HEAP_H
